class CreatePaymentTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_transactions do |t|

      t.integer :influencer_id
      t.belongs_to :glorify
      t.boolean :paid, default: false
      t.float :amount

      t.integer :stripe_charge_id
      t.jsonb :other_details, default: {}
      t.jsonb :refund_details, default: {}

      t.timestamps
    end
  end
end
