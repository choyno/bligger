class UpdateIntegerAmountToDecimal < ActiveRecord::Migration[5.0]
  def change
    change_column :glorifies, :amount, :decimal
    change_column :payment_transactions, :amount, :decimal
  end
end
