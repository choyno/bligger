class CreateGlorifies < ActiveRecord::Migration[5.0]
  def change
    create_table :glorifies do |t|

      t.belongs_to :user
      t.belongs_to :post

      t.integer :amount
      t.jsonb :other_details, default: {}

      t.timestamps
    end
  end
end
