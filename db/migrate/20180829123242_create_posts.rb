class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|

      t.belongs_to :user
      t.string :title, limit: 255
      t.text :contents

      t.timestamps
    end
  end
end
