class AddingOtherDetailsInPostTable < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :other_details, :jsonb, default: {}
  end
end
