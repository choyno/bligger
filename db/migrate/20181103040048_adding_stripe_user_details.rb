class AddingStripeUserDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :stripe_customer_id, :string
    add_column :users, :stripe_account_id, :string
    add_column :users, :stripe_account_secret_key, :string
    add_column :users, :stripe_account_publishable_key, :string
  end
end
