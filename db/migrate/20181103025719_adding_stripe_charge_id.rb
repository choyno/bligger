class AddingStripeChargeId < ActiveRecord::Migration[5.0]
  def change
    add_column :glorifies, :stripe_charge_id, :string
    add_column :glorifies, :amount_in_cent, :decimal, precision: 8, scale: 2, default: 0.0,   null: false
  end
end
