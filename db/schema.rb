# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181103040048) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "glorifies", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.decimal  "amount"
    t.jsonb    "other_details",                            default: {}
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.string   "stripe_charge_id"
    t.decimal  "amount_in_cent",   precision: 8, scale: 2, default: "0.0", null: false
    t.index ["post_id"], name: "index_glorifies_on_post_id", using: :btree
    t.index ["user_id"], name: "index_glorifies_on_user_id", using: :btree
  end

  create_table "payment_transactions", force: :cascade do |t|
    t.integer  "influencer_id"
    t.integer  "glorify_id"
    t.boolean  "paid",             default: false
    t.decimal  "amount"
    t.integer  "stripe_charge_id"
    t.jsonb    "other_details",    default: {}
    t.jsonb    "refund_details",   default: {}
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["glorify_id"], name: "index_payment_transactions_on_glorify_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title",         limit: 255
    t.text     "contents"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "image"
    t.jsonb    "other_details",             default: {}
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                      default: "", null: false
    t.string   "username",                       limit: 100, default: "", null: false
    t.string   "encrypted_password",                         default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                              default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "user_type",                                  default: 1
    t.string   "stripe_customer_id"
    t.string   "stripe_account_id"
    t.string   "stripe_account_secret_key"
    t.string   "stripe_account_publishable_key"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

end
