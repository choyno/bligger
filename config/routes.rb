Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :users, only: [:session, :registration]

  devise_scope :user do
    get 'admin/login', to: 'devise/sessions#new'
    authenticated :user do
      root :to => 'checkroot#index'
    end
    unauthenticated :user do
      root :to => 'posts#index'
    end
  end

  #public
  resources :posts, only:[:index, :show] do
    collection do
      get :archive
    end
  end

  #admin
  namespace :admin do
    resources :posts
    resources :payments
  end
  resources :admin, only:[:index]

  namespace :manage do
    resources :glorifies
    resources :listings
    resources :charges, only: [:create]
  end
  resources :manage, only:[:index]




end
