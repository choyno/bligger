ActiveAdmin.register Post, as: 'Published Posts' do

  #config.filters = false
  scope :all
  scope :publish_on
  scope :publish_off

  actions :index, :show, :update

  action_item :publish, only: :show do
    link_to 'Publish "ON"', publish_admin_published_post_path(resource.id), method: :put  if (resource.is_published == "off")
  end

  action_item :unpublish, only: :show do
    link_to 'Publish "Off"', unpublish_admin_published_post_path(resource.id), method: :put if(resource.is_published == "on")
  end

  member_action :publish, method: :put do
    post = Post.find(params[:id])
    post.other_details["is_published"] = "on"
    post.save
    redirect_to  admin_published_post_path(post)
  end

  member_action :unpublish, method: :put do
    post = Post.find(params[:id])
    post.other_details["is_published"] = "off"
    post.save
    redirect_to admin_published_post_path(post)
  end

  index do
    Post.column_names.each do |col|
      if col == "other_details"
        column 'other_details' do |post|
          post_info = Post.find(post.id)
          post_info.is_published
        end
      else
        column col
      end
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :image do |ad|
        image_tag ad.image.url
      end
      row :created_at
      row :other_details do
        resource.is_published
      end
    end
  end

end

