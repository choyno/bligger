class FeaturedImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  #include CarrierWave::RMagick

  include Cloudinary::CarrierWave
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  #storage :cloudinary

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w(jpg jpeg png)
  end
end
