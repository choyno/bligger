module ApplicationHelper

  def has_more_option(post_count)
     return (post_count > 5)
  end

  def page_counter(details)
    return "#{details.current_page}/#{@details.total_pages}"
  end

end
