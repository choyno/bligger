class Post < ApplicationRecord

  belongs_to :user
  has_many :glorifies

  mount_uploader :image, FeaturedImageUploader

  validates :image, :presence => true
  validates :title, :presence => true
  validates :contents, :presence => true

  scope :publish_on, (lambda do
    where("other_details -> 'is_published' ? :status", status: 'on')
  end)

  scope :publish_off, (lambda do
    where("other_details -> 'is_published' ? :status ", status: 'off')
  end)


  def self.order_recent
    self.order("created_at DESC")
  end

  def display_post_date
    self.date_format("%d %b, %Y")
  end

  def common_post_date
    self.date_format("%Y-%m-%d")
  end

  def date_format(format)
    return self.try(:created_at).strftime(format)
  end

  def image_name
    if self.image.present?
      self.image.file.identifier
    end
  end

  def is_published
    other_details["is_published"] || "off"
  end

  def update_is_published( status)
    other_details["is_published"] = status
    save
  end

  def total_count
    self.glorifies.count
  end

  def total_glorified_amount
    total = self.glorifies.sum('glorifies.amount')
    "#{total}$"
  end

end
