class PaymentTransaction < ApplicationRecord

  belongs_to :user, foreign_key: :influencer_id
  belongs_to :glorify, optional: true

  scope :pending_payment, (lambda do
    where(paid: false)
  end)

  scope :paid_payment, (lambda do
    where(paid: true)
  end)

  scope :total_amount , (lambda do
    self.sum('payment_transactions.amount')
  end)

end
