class Glorify < ApplicationRecord

  belongs_to :user
  belongs_to :post
  has_many :payment_transactions

  accepts_nested_attributes_for :payment_transactions

end

