class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :posts
  has_many :glorifies
  has_many :glorify_posts, :through => :glorifies, :source => :post

  has_many :payments, class_name: :PaymentTransaction, foreign_key: :influencer_id

  belongs_to :post

  enum user_type: { glorifier: 0, influencer: 1, master: 4}

  def glorifier?
    self.user_type == "glorifier"
  end

  def influencer?
    self.user_type == "influencer"
  end

  def total_pending_payment_amount
    total = self.payments.pending_payment.total_amount
  end

  def total_paid_payment_amount
    total = self.payments.paid_payment.total_amount
  end


end
