class PostsController < ApplicationController

  def index
    posts = Post.order_recent
    @posts = posts.limit(5)
    @post_count = posts.count
  end

  def archive
    @posts = Post.order_recent.page(page_param).per(5)
  end

  def show
    @post = Post.find(post_id)
  end

  private

  def post_id
    params[:id]
  end

  def page_param
    params[:page] || 1
  end

end
