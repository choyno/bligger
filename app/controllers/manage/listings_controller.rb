class Manage::ListingsController < ManageController

  def index
    posts = Post.order_recent
    @posts = posts.limit(20)
    @post_count = posts.count
  end

  def show
    @listing = Post.find(post_id)
    @glorify = current_user.glorifies.new
  end

  private

  def post_id
    params[:id]
  end


end
