class Manage::ChargesController < ManageController

  def  create

    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    )

    @glorify = current_user.glorifies.new(glorify_params)

    #convert to cents and roundoff
    amount_to_cent = ((@glorify.amount).to_f * 100).round

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => amount_to_cent,
      :description => "Glorify #{@glorify.post.try(:title)}",
      :currency    => 'usd'
    )

    @glorify.amount_in_cent = amount_to_cent
    @glorify.stripe_charge_id= charge.id
    @glorify.other_details = charge

    @glorify.payment_transactions.build({influencer_id: influencer_id, amount: @glorify.amount })

    if @glorify.save
      flash[:success] = "Successfully Glorified."
      redirect_to manage_glorifies_path
    else
      render :show
    end

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path


  end

  def influencer_id
    params[:influencer_id]
  end

  def glorify_params
    params.require(:glorify).permit!
  end

end
