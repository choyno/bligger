class Manage::GlorifiesController < ManageController

  def index
    @glorifies = current_user.glorify_posts.uniq
  end

end
