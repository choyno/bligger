class CheckrootController < ApplicationController

  before_filter :authenticate_user!

  def index
    redirect_to path_finder
  end

  private

  def path_finder
    if current_user.glorifier?
      Rails.logger.info "Glorifier"
      manage_glorifies_path
    elsif current_user.influencer?
      Rails.logger.info "influencer"
      admin_posts_path
    else
      posts_path
    end
  end

end
