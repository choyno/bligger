class AdminController < ApplicationController

  before_action :authenticate_user!
  layout "application_admin"

  def index
    redirect_to admin_posts_path
  end

end
