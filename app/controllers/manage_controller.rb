class ManageController < ApplicationController

  before_action :authenticate_user!
  layout "application_buyer"

  def index
    redirect_to manage_glories_path
  end

end
