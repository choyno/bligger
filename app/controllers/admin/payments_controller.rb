class Admin::PaymentsController < AdminController

  def index
    @available_amount = current_user.total_pending_payment_amount
    @paid_amount = current_user.total_paid_payment_amount
  end

  def create
  end

  private

end
