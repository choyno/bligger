class Admin::PostsController < AdminController

  def index
    @posts = current_user.posts.order_recent
  end

  def new
    @post = current_user.posts.new
  end

  def create
    @post = current_user.posts.new(post_params)
    @post.other_details[:is_published] = "on"
    @post.other_details[:create_by] = current_user.username

    if @post.save
      flash[:success] = "Successfully Created."
      redirect_to admin_posts_path
    else
      render :new
    end
  end

  def edit
    post_info
  end

  def update
    if post_info.update_attributes(post_params.except("other_details"))

       is_published = post_params["other_details"]["is_published"]
       post_info.update_is_published( is_published )
       redirect_to edit_admin_post_path(post_info), notice: "Successfully Updated."

    else
      render :edit
    end
  end

  private

  def post_params
    params.require(:post).permit!
  end

  def post_id
    params[:id]
  end

  def post_info
    @post = current_user.posts.find( post_id )
  end

end
